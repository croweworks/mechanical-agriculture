package com.croweworks.base;

import com.croweworks.base.block.BaseBlocks;
import com.croweworks.base.item.BaseItems;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(Base.MOD_ID)
public class Base
{
    public static final Logger LOGGER = LogManager.getLogger();
    public static final String MOD_ID = "base";

    public Base() {
        final IEventBus bus = FMLJavaModLoadingContext.get().getModEventBus();
        bus.addListener(this::setup);

        BaseItems.register(bus);
        BaseBlocks.register(bus);
//        DefendersTrapBlocks.register(bus);

        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event)
    {

    }
}
