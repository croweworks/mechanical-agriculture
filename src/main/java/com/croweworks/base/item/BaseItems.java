package com.croweworks.base.item;

import com.croweworks.base.Base;
import net.minecraft.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class BaseItems {

    public static final DeferredRegister<Item> ITEMS =
            DeferredRegister.create(ForgeRegistries.ITEMS, Base.MOD_ID);

    public static final RegistryObject<Item> BASE_ITEM = ITEMS.register("base_item",
            () -> new Item(new Item.Properties().group(BaseItemGroup.BASE_ITEM_GROUP)));

    public static void register(final IEventBus eventBus) {
        ITEMS.register(eventBus);
    }
}
