package com.croweworks.base.item;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class BaseItemGroup {

    public static final ItemGroup BASE_ITEM_GROUP = new ItemGroup("baseTab") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(BaseItems.BASE_ITEM.get());
        }
    };
}
